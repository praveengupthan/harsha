<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Harsha Auto Group</title>

    <?php include 
        'includes/styles.php'
    ?>  
    <!--/ styles -->
</head>
<body>
   <?php
        include 'includes/header.php';
        include 'includes/arrayObjects.php'
   ?>
    <!-- main-->
    <!-- video -->
    <div class="video-section">
      <video autoplay muted loop>
        <source src="img/Night-Traffic.mp4" type="video/mp4">
        Your browser does not support HTML5 video.
      </video>
      <a href="javascript:void(0)" class="move-top-video"><span class="icon-angle-double-down icomoon"></span></a>
    </div>
    <!--/ video -->
    <!-- about and group of companies  -->
    <div class="main-section">
        <!-- row -->
        <div class="row no-gutters">
            <!-- about col -->
            <div class="col-md-6 about-section">

              <article>
                <h1 class="h1">About Harsha Auto</h1>
                <p>Sri. Muppavarapu Harshavardhan is a young dynamic Entrepreneur graduated in Commerce and hails from a respectable family with a public image. He is the native of Nellore, Andhra Pradesh and presently residing at Hyderabad. He is well known business leader in Automobile and hospitality industries. His unshakable belief that India will never achieve its true growth story until the rural sector of the country is empowered to make choices and transform their own lives. </p>
                <a href="javascript:void(0)" class="brd-link text-uppercase">Read More</a>
              </article>

            </div>
            <!--/ about col -->

            <!-- image gallery col -->
            <div class="col-md-6 homeSlider">
                 <!-- slider -->
                <div class="CarouselGroup">
                    <!-- Swiper -->
                    <div class="swiper-container groupSwiper">
                        <div class="swiper-wrapper">
                            <?php 
                            for($i=0; $i<count($homePageSliderItem); $i++) { ?>
                            <div class="swiper-slide">
                                <img src="img/homeSlider/<?php echo $homePageSliderItem[$i][0]?>" alt="" class="img-fluid">
                            </div>    
                            <?php }?>            
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                    <!--/ swiper -->
                </div>
                <!--/ slider -->
            </div>
            <!--/ image gallery col -->           
        </div>
        <!--/ row -->
    </div>
    <!--/ about and group of companies-->

    <!-- partners -->
    <div class="partners mt-3">
        <!-- row -->
        <div class="row no-gutters">
            <!-- col -->
            <div class="col-md-4 partnerstitle">
                <article>
                    <h2 class="h1">Our Top Partners</h1>
                      <p>A name of trust for many leading automobile brands like Toyota, Hero MotoCorp, Bharat Benz, Volvo etc.</p>
                </article>
            </div>
            <!--/ col -->
              <!-- col -->
              <div class="col-md-8 brandscol">
                  <!-- Swiper -->
                  <div class="swiper-container brands-container">
                      <div class="swiper-wrapper">
                        <div class="swiper-slide"><img src="img/brands/4.png" alt="" title=""></div>
                        <div class="swiper-slide"><img src="img/brands/3.png" alt="" title=""></div>
                        <div class="swiper-slide"><img src="img/brands/7.png" alt="" title=""></div>
                        <div class="swiper-slide"><img src="img/brands/8.png" alt="" title=""></div>
                        <div class="swiper-slide"><img src="img/brands/2.png" alt="" title=""></div>
                        <div class="swiper-slide"><img src="img/brands/9.png" alt="" title=""></div>
                      </div>
                      <!-- Add Pagination -->
                      <div class="swiper-pagination"></div>
                  </div>
                  <!--/ swiper -->
                </div>
              <!--/ col -->
        </div>
        <!--/ row -->
    </div>
    <!--/ partners -->

    <!-- trusts -->
    <div class="trustsHome py-4">
          <!-- container -->
          <div class="container">
            <!-- row -->
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="section-title text-center">Our Trusts</h1>
                </div>
            </div>
            <!--/ row -->

            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-md-6">
                    <figure>
                        <img src="img/MuppFoundation.jpg" alt="" class="img-fluid">
                    </figure>
                    <article class="p-3 text-center">
                        <h5 class="h5 fsbold">Sri Muppavarapu Foundation</h5>
                        <p>Sri Muppavarapu Foundation, a charitable, Non - Profit organization in Nellore District is highly dedicated to promoting empowerment of people of Rural India by providing its enormous humanitarian services regardless of their social, religious or economic factors.</p>
                        <a href="http://www.srimuppavarapufoundation.co.in/" target="_blank" class="bluebrdLink">Read More</a>
                    </article>
                </div>
                <!--/ col -->

                  <!-- col -->
                  <div class="col-md-6">
                    <figure>
                        <img src="img/swarnaBharatTrustHomeImg.jpg" alt="" class="img-fluid">
                    </figure>
                    <article class="p-3 text-center">
                        <h5 class="h5 fsbold">Swarna Bharat Trust</h5>
                        <p>Swarna Bharat Trust, a service oriented non-governmental organization, was born in the year 2001 with the noble intentions of eight well-meaning, like-minded and service oriented individuals; all friends of Sri Muppavarapu Venkaiah Naidu, Hon’ble Vice President of India</p>
                        <a href="https://swarnabharathtrust.org.in/" target="_blank" class="bluebrdLink">Read More</a>
                    </article>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
          </div>
          <!--/ container -->
    </div>
    <!--/ trusts -->   

    <!-- events -->
    <div class="latest-events">
        <!-- container -->
        <div class="container">

                <!-- row -->
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="section-title text-center">CSR Activities</h1>
                </div>
            </div>
            <!--/ row -->

                <!-- swiper Parent div -->
                <div class="GroupItemSwiper pb-2">
                     <!-- Swiper -->
                    <div class="swiper-container newVehicles py-3">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="javascript:void(0)"><img src="img/activity/act01.jpg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                            <a href="javascript:void(0)"><img src="img/activity/act02.jpg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                            <a href="javascript:void(0)"><img src="img/activity/act03.jpg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                            <a href="javascript:void(0)"><img src="img/activity/act04.jpg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                            <a href="javascript:void(0)"><img src="img/activity/act05.jpg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                            <a href="javascript:void(0)"><img src="img/activity/act06.jpg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                            <a href="javascript:void(0)"><img src="img/activity/act07.jpg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                            <a href="javascript:void(0)"><img src="img/activity/act08.jpg" alt="" title="" class="img-fluid"></a>
                            </div>   
                            <div class="swiper-slide">
                            <a href="javascript:void(0)"><img src="img/activity/act09.jpg" alt="" title="" class="img-fluid"></a>
                            </div>                        
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                    <!--/ swiper -->
                </div>
                <!-- swiper Parent div -->
       
        </div>
        <!--/ container -->
    </div>
    <!--/ events-->

   
    <!--/ main -->
   <?php
    include 'includes/footer.php' 
   ?>   

   <?php
    include 'includes/scripts.php'
   ?>

</body>
</html>
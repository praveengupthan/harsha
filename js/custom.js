//add class to header on scroll
$(window).scroll(function(){
    if($(this).scrollTop() > 20){
        $('.fixed-top').addClass('fixed-theme', 1000);
        
    }else{
        $('.fixed-top').removeClass('fixed-theme', 1000);
    }
});

baguetteBox.run('.grid-gallery', { animation: 'slideIn'});

//add class to header on scroll
$(window).scroll(function(){
  if($(this).scrollTop() > 50){
      $('.ashokaChakra').addClass('DynamicChakra');
      
  }else{
      $('.ashokaChakra').removeClass('DynamicChakra');
  }
});


//on click move to browser top
$(document).ready(function(){
    $(window).scroll(function(){
        if($(this).scrollTop() > 50){
            $('#moveTop').fadeIn()
        }else{
            $('#moveTop').fadeOut();
        }
    });

    //click event to scroll to top
    $('#moveTop').click(function(){
        $('html, body').animate({scrollTop:0}, 400);
    });

    $(".move-top-video").click(function() {
      $('html,body').animate({
          scrollTop: $(".main-section").offset().top},
          'slow');               
      });
      
     

  });

 

//initilizing the brands swiper
var swiper = new Swiper('.brands-container', {
  slidesPerView: 5,
  spaceBetween: 10,
 autoplay: {
    delay: 5000,
    disableOnInteraction: true,
  },
  // init: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 40,
    },
    1024: {
      slidesPerView: 4,
      spaceBetween: 10,
    },
  }
});


//group item banner
var swiper = new Swiper('.groupSwiper', {
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

//groupItem
var swiper = new Swiper('.newVehicles', {
  slidesPerView: 5,
  spaceBetween: 10,
 autoplay: {
    delay: 8000,
    disableOnInteraction: true,
  },
  // init: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 40,
    },
    1024: {
      slidesPerView: 4,
      spaceBetween: 10,
    },
  }
});


//groupItem
var swiper = new Swiper('.newEvents', {
  slidesPerView: 5,
  spaceBetween: 10,
 autoplay: {
    delay: 7000,
    disableOnInteraction: true,
  },
  // init: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 4,
      spaceBetween: 40,
    },
    1024: {
      slidesPerView: 4,
      spaceBetween: 10,
    },
  }
});

//groupItem
var swiper = new Swiper('.offersSwiper', {
  slidesPerView: 5,
  spaceBetween: 10,
 autoplay: {
    delay: 6000,
    disableOnInteraction: true,
  },
  // init: false,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 1,
      spaceBetween: 40,
    },
    1024: {
      slidesPerView: 2,
      spaceBetween: 10,
    },
  }
});

















   
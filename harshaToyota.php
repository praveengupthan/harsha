<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Harsha Auto Group</title>

    <?php include 
        'includes/styles.php'
    ?>     
    <!--/ styles -->
</head>
<body>

   <?php
        include 'includes/header.php';
        include 'includes/arrayObjects.php'
   ?>

    <!-- main-->  
    <!-- about and group of companies  -->
    <div class="subPageMain">
        <!-- slider -->
        <div class="CarouselGroup">
            <!-- Swiper -->
            <div class="swiper-container groupSwiper">
                <div class="swiper-wrapper">
                    <?php 
                    for($i=0; $i<count($harshaSliderItem); $i++) { ?>
                    <div class="swiper-slide">
                        <img src="img/harshaBan/<?php echo $harshaSliderItem[$i][0]?>" alt="" class="img-fluid">
                    </div>    
                    <?php }?>            
                </div>
                <!-- Add Arrows -->
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <!--/ swiper -->
        </div>
        <!--/ slider -->

        <!-- page title -->
        <section class="pageHeader">
            <!-- container -->
            <div class="container">
                <h1>Harsha Auto</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">Group of Companies</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Harsha Toyota</li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </section>
        <!--/ page title -->

        <!-- page body -->
        <section class="pageBody">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row pb-4">
                    <!-- col -->
                    <div class="col-md-6">
                        <img src="img/harshaAboutimg.jpg" alt="" class="img-fluid w-100">
                        <div class="justify-content-center d-flex pt-4">
                            <a href="javascript:void(0)" class="bluebrdLink">Book Now</a>
                            <a href="javascript:void(0)" class="bluebrdLink mx-1">Services</a>
                            <a href="javascript:void(0)" class="bluebrdLink">Test Drive</a>
                        </div>
                    </div>
                    <!-- col -->

                    <!-- col -->
                    <div class="col-md-6">
                         <h2 class="h3 fsbold">Welcome to Harsha Auto</h2>
                         <p>The only dealer partner in India which won the confidence of Toyota and able to be the reliable channel partner for Toyota in three states of India i.e. Andhra Pradesh, Telangana & Tamil Nadu for sale and service of Toyota cars. Equipped with state of the art infrastructure facilities best in its class (all in one roof concept like sales, spares, service and body shop in one place), well-trained and committed manpower and through focus on customer satisfaction by delivering quality service. Placing customer satisfaction first, integrating sales with service and service parts in a single convenient location, we contribute to speedy and efficient service, allowing customers to experience the convenience and pleasure of owning Toyota automobile.</p>
                    </div>
                    <!-- col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->


            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row titleRow py-3">
                    <div class="col-md-12 d-flex justify-content-between">
                        <h3 class="h5 fsbold">New Cars</h3>
                        <a href="javascript:void(0)" class="bluebrdLink mx-1">View All Cars</a>
                    </div>
                </div>
                <!--/ row -->

                <!-- swiper Parent div -->
                <div class="GroupItemSwiper py-3">
                     <!-- Swiper -->
                    <div class="swiper-container newVehicles py-3">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <a href="javascript:void(0)"><img src="img/data/gallery/1.jpeg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)"><img src="img/data/gallery/2.jpeg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)"><img src="img/data/gallery/12.jpeg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)"><img src="img/data/gallery/15.jpeg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)"><img src="img/data/gallery/13.jpeg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)"><img src="img/data/gallery/12.jpeg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)"><img src="img/data/gallery/15.jpeg" alt="" title="" class="img-fluid"></a>
                            </div>
                            <div class="swiper-slide">
                                <a href="javascript:void(0)"><img src="img/data/gallery/13.jpeg" alt="" title="" class="img-fluid"></a>
                            </div>                           
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                    <!--/ swiper -->
                </div>
                <!-- swiper Parent div -->


                 <!-- row -->
                 <div class="row titleRow py-3">
                    <div class="col-md-12 d-flex justify-content-between">
                        <h3 class="h5 fsbold">Events</h3>
                        <a href="javascript:void(0)" class="bluebrdLink mx-1">View All Events</a>
                    </div>
                </div>
                <!--/ row -->
                 <!-- swiper Parent div -->
                 <div class="GroupItemSwiper py-3">
                     <!-- Swiper -->
                    <div class="swiper-container newEvents py-3">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="card blogcard">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top img-fluid img-hover" src="img/data/car_08.jpg" alt="">
                                    </a>
                                    <div class="card-body position-relative">                           
                                        <h6>Event Name will be here</h6>
                                        <p>
                                            <small class="fgray">Posted on July 21 2020</small>
                                        </p>
                                        <a class="d-inline-block round-link" href="javascript:void(0)"><span class="icon-chevron-right icomoon"></span></a>                           
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card blogcard">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top img-fluid img-hover" src="img/data/event03.jpg" alt="">
                                    </a>
                                    <div class="card-body position-relative">                           
                                        <h6>Event Name will be here</h6>
                                        <p>
                                            <small class="fgray">Posted on July 21 2020</small>
                                        </p>
                                        <a class="d-inline-block round-link" href="javascript:void(0)"><span class="icon-chevron-right icomoon"></span></a>                           
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card blogcard">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top img-fluid img-hover" src="img/data/event04.jpg" alt="">
                                    </a>
                                    <div class="card-body position-relative">                           
                                        <h6>Event Name will be here</h6>
                                        <p>
                                            <small class="fgray">Posted on July 21 2020</small>
                                        </p>
                                        <a class="d-inline-block round-link" href="javascript:void(0)"><span class="icon-chevron-right icomoon"></span></a>                           
                                    </div>
                                </div>                               
                            </div>
                            <div class="swiper-slide">
                                <div class="card blogcard">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top img-fluid img-hover" src="img/data/car_08.jpg" alt="">
                                    </a>
                                    <div class="card-body position-relative">                           
                                        <h6>Event Name will be here</h6>
                                        <p>
                                            <small class="fgray">Posted on July 21 2020</small>
                                        </p>
                                        <a class="d-inline-block round-link" href="javascript:void(0)"><span class="icon-chevron-right icomoon"></span></a>                           
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card blogcard">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top img-fluid img-hover" src="img/data/event03.jpg" alt="">
                                    </a>
                                    <div class="card-body position-relative">                           
                                        <h6>Event Name will be here</h6>
                                        <p>
                                            <small class="fgray">Posted on July 21 2020</small>
                                        </p>
                                        <a class="d-inline-block round-link" href="javascript:void(0)"><span class="icon-chevron-right icomoon"></span></a>                           
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card blogcard">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top img-fluid img-hover" src="img/data/event04.jpg" alt="">
                                    </a>
                                    <div class="card-body position-relative">                           
                                        <h6>Event Name will be here</h6>
                                        <p>
                                            <small class="fgray">Posted on July 21 2020</small>
                                        </p>
                                        <a class="d-inline-block round-link" href="javascript:void(0)"><span class="icon-chevron-right icomoon"></span></a>                           
                                    </div>
                                </div>                               
                            </div>
                            <div class="swiper-slide">
                                <div class="card blogcard">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top img-fluid img-hover" src="img/data/car_08.jpg" alt="">
                                    </a>
                                    <div class="card-body position-relative">                           
                                        <h6>Event Name will be here</h6>
                                        <p>
                                            <small class="fgray">Posted on July 21 2020</small>
                                        </p>
                                        <a class="d-inline-block round-link" href="javascript:void(0)"><span class="icon-chevron-right icomoon"></span></a>                           
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card blogcard">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top img-fluid img-hover" src="img/data/event03.jpg" alt="">
                                    </a>
                                    <div class="card-body position-relative">                           
                                        <h6>Event Name will be here</h6>
                                        <p>
                                            <small class="fgray">Posted on July 21 2020</small>
                                        </p>
                                        <a class="d-inline-block round-link" href="javascript:void(0)"><span class="icon-chevron-right icomoon"></span></a>                           
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="card blogcard">
                                    <a href="javascript:void(0)">
                                        <img class="card-img-top img-fluid img-hover" src="img/data/event04.jpg" alt="">
                                    </a>
                                    <div class="card-body position-relative">                           
                                        <h6>Event Name will be here</h6>
                                        <p>
                                            <small class="fgray">Posted on July 21 2020</small>
                                        </p>
                                        <a class="d-inline-block round-link" href="javascript:void(0)"><span class="icon-chevron-right icomoon"></span></a>                           
                                    </div>
                                </div>                               
                            </div>                       
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                    <!--/ swiper -->
                </div>
                <!-- swiper Parent div -->

                
                 <!-- row -->
                 <div class="row titleRow py-3">
                    <div class="col-md-12 d-flex justify-content-between">
                        <h3 class="h5 fsbold">Offers</h3>
                        <a href="javascript:void(0)" class="bluebrdLink mx-1">View All Offers</a>
                    </div>
                </div>
                <!--/ row -->
                 <!-- swiper Parent div -->
                 <div class="GroupItemSwiper py-3">
                     <!-- Swiper -->
                    <div class="swiper-container offersSwiper py-3">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                               <a href="javascript:void(0)"><img src="img/offer01.png" alt="img-fluid w-100"></a>
                            </div>
                            <div class="swiper-slide">
                               <a href="javascript:void(0)"><img src="img/offer02.png" alt="img-fluid w-100"></a>
                            </div>
                            <div class="swiper-slide">
                               <a href="javascript:void(0)"><img src="img/offer03.png" alt="img-fluid w-100"></a>
                            </div>
                            <div class="swiper-slide">
                               <a href="javascript:void(0)"><img src="img/offer04.png" alt="img-fluid w-100"></a>
                            </div>
                                               
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                    <!--/ swiper -->
                </div>
                <!-- swiper Parent div -->
            </div>
            <!--/ container -->

             <!-- latest gallery -->
            <div class="latest-gallery gallery-block grid-gallery">
                <!-- row -->
                <div class="row no-gutters">
                <div class="col-lg-12 text-center">
                    <h3 class="h4 text-center py-3 fblue">Latest Gallery</h1>
                </div>
                </div>
                <!--/ row -->
                <!-- gallery images -->
                <div class="gallery-images">
                <!-- row -->
                <div class="row no-gutters">
                    <div class="col-md-2 item">
                        <a class="lightbox" href="img/data/gallery/1.jpeg">
                            <img class="img-fluid image scale-on-hover" src="img/data/gallery/1.jpeg">
                        </a>
                    </div>
                    <div class="col-md-2 item">
                        <a class="lightbox" href="img/data/gallery/2.jpeg">
                            <img class="img-fluid image scale-on-hover" src="img/data/gallery/2.jpeg">
                        </a>
                    </div>
                    <div class="col-md-2 item">
                        <a class="lightbox" href="img/data/gallery/3.jpeg">
                            <img class="img-fluid image scale-on-hover" src="img/data/gallery/3.jpeg">
                        </a>
                    </div>
                    <div class="col-md-2 item">
                        <a class="lightbox" href="img/data/gallery/4.jpeg">
                            <img class="img-fluid image scale-on-hover" src="img/data/gallery/4.jpeg">
                        </a>
                    </div>
                    <div class="col-md-2 item">
                        <a class="lightbox" href="img/data/gallery/5.jpeg">
                            <img class="img-fluid image scale-on-hover" src="img/data/gallery/5.jpeg">
                        </a>
                    </div>
                    <div class="col-md-2 item">
                        <a class="lightbox" href="img/data/gallery/6.jpeg">
                            <img class="img-fluid image scale-on-hover" src="img/data/gallery/6.jpeg">
                        </a>
                    </div>
                    <div class="col-md-2 item">
                        <a class="lightbox" href="img/data/gallery/7.jpeg">
                            <img class="img-fluid image scale-on-hover" src="img/data/gallery/7.jpeg">
                        </a>
                    </div>
                    <div class="col-md-2 item">
                        <a class="lightbox" href="img/data/gallery/8.jpeg">
                            <img class="img-fluid image scale-on-hover" src="img/data/gallery/8.jpeg">
                        </a>
                    </div>
                    <div class="col-md-2 item">
                        <a class="lightbox" href="img/data/gallery/9.jpeg">
                            <img class="img-fluid image scale-on-hover" src="img/data/gallery/9.jpeg">
                        </a>
                    </div>
                    <div class="col-md-2 item">
                    <a class="lightbox" href="img/data/gallery/10.jpeg">
                        <img class="img-fluid image scale-on-hover" src="img/data/gallery/10.jpeg">
                    </a>
                </div>
                <div class="col-md-2 item">
                    <a class="lightbox" href="img/data/gallery/11.jpeg">
                        <img class="img-fluid image scale-on-hover" src="img/data/gallery/11.jpeg">
                    </a>
                </div>
                <div class="col-md-2 item">
                <a class="lightbox" href="img/data/gallery/12.jpeg">
                    <img class="img-fluid image scale-on-hover" src="img/data/gallery/12.jpeg">
                </a>
            </div>
                </div>
                <!--/ row -->
                </div>
                <!--/ gallery images -->
            </div>
            <!--/ latest gallery -->

            <!-- container -->
            <div class="container">
                 <!-- row -->
                 <div class="row titleRow py-3">
                    <div class="col-md-12 d-flex justify-content-between">
                        <h3 class="h5 fsbold">Videos Gallery</h3>
                        <a href="javascript:void(0)" class="bluebrdLink mx-1">View All</a>
                    </div>
                </div>
                <!--/ row -->

                <!-- swiper Parent div -->
                <div class="GroupItemSwiper py-3">
                     <!-- Swiper -->
                    <div class="swiper-container newVehicles py-3">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <iframe width="100%" height="200" src="https://www.youtube.com/embed/jsrJKybBwm0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="swiper-slide">
                                <iframe width="100%" height="200" src="https://www.youtube.com/embed/jsrJKybBwm0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="swiper-slide">
                                <iframe width="100%" height="200" src="https://www.youtube.com/embed/jsrJKybBwm0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="swiper-slide">
                                <iframe width="100%" height="200" src="https://www.youtube.com/embed/jsrJKybBwm0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="swiper-slide">
                                <iframe width="100%" height="200" src="https://www.youtube.com/embed/jsrJKybBwm0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>                                            
                        </div>
                        <!-- Add Pagination -->
                        <div class="swiper-pagination"></div>
                    </div>
                    <!--/ swiper -->
                </div>
                <!-- swiper Parent div -->
            </div>
            <!--/ container -->



        </section>
        <!--/ page body -->


    </div>
    <!--/ about and group of companies-->

   

    

   

    
    <!--/ main -->
   <?php
    include 'includes/footer.php' 
   ?>   

   <?php
    include 'includes/scripts.php'
   ?>

</body>
</html>
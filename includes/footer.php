 <!-- footer -->
 <footer>
      <a href="javascript:void(0)" class="moveTop" id="moveTop"><span class="icon-arrow-up icomoon"></span></a>
        <!--top footer -->
        <div class="top-footer">
           <!-- container -->
          <div class="container">
              <ul class="nav justify-content-center">
                  <li class="nav-item"><a href="javascript:void(0)" class="nav-link">Home</a></li>
                  <li class="nav-item"><a href="javascript:void(0)" class="nav-link">About</a></li>
                  <li class="nav-item"><a href="javascript:void(0)" class="nav-link">Groups</a></li>
                  <li class="nav-item"><a href="javascript:void(0)" class="nav-link">Blog</a></li>
                  <li class="nav-item"><a href="javascript:void(0)" class="nav-link">Careers</a></li>
                  <li class="nav-item"><a href="javascript:void(0)" class="nav-link">Contact</a></li>
                  <li class="nav-item"><a href="javascript:void(0)" class="nav-link">HRMS</a></li>
              </ul>
              <div class="footer-social text-center">
                  <a href="javascript:void(0)" target="_blank"><span class="icon-facebook icomoon"></span></a>
                  <a href="javascript:void(0)" target="_blank"><span class="icon-twitter icomoon"></span></a>
                  <a href="javascript:void(0)" target="_blank"><span class="icon-linkedin icomoon"></span></a>
              </div>            
          </div>
          <!--/ container -->
        </div>
        <!--/ top footer -->

        <!--top footer -->
        <div class="bottom-footer">
          <!-- container -->
         <div class="container text-center">
              <p><i><small>Copyright © 2020 Harsha Auto Pvt Ltd. All rights reserved.</small></i></p>
         </div>
         <!--/ container -->
       </div>
       <!--/ top footer -->
    </footer>
    <!--/ footer -->
 <!-- header -->
 <header class="bsnav-sticky bsnav-sticky-slide bsnav">
        <div class="navbar navbar-expand-lg  custom-container">
            <a class="navbar-brand" href="#"><img src="img/logo.svg" alt=""></a>
            <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-md-end">
              <ul class="navbar-nav navbar-mobile mx-auto">
                <li class="nav-item active"><a class="nav-link" href="index.php">Home</a></li>
                <li class="nav-item"><a class="nav-link" href="#">About</a></li>
                <li class="nav-item dropdown zoom"><a class="nav-link" href="#">Group of Companies <i class="caret"></i></a>
                  <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="harshaToyota.php">Harsha Toyota</a></li>                   
                    <li class="nav-item"><a class="nav-link" href="#">Sri Harsha Hero</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Sri Harsha Trucking</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Sri Harsha Volvo</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Vishwarupa Automotive</a></li>
                  </ul>
                </li>
                <li class="nav-item"><a class="nav-link" href="#">Blog</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Careers</a></li>
                <li class="nav-item"><a class="nav-link" href="#">Contact</a></li>
                <li class="nav-item"><a class="nav-link" href="#">HRMS</a></li>
              </ul>

              <ul class="navbar-nav navbar-mobile header-social">
                <li class="nav-item"><a class="nav-link" href="#" target="_blank"><span class="icon-facebook icomoon"></span></a></li>
                <li class="nav-item"><a class="nav-link" href="#" target="_blank"><span class="icon-twitter icomoon"></span></a></li>
                <li class="nav-item"><a class="nav-link" href="#" target="_blank"><span class="icon-linkedin icomoon"></span></a></li>               
              </ul>
            </div>
          </div>         
         
          <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar"></div>
          </div>
    </header>
    <!--/ header -->
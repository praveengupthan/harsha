<link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
<!-- styles -->
<link rel="stylesheet" href="css/bootstrap-material.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/bsnav.min.css">  
<link rel="stylesheet" href="css/swiper.min.css"> 
<link rel="stylesheet" href="css/baguetteBox.css"> 
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/fakeloader.css">